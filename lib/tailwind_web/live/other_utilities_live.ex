defmodule TailwindWeb.OtherUtilitiesLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="flex flex-col items-center justify-center p-6 bg-gray-200">
      <div class="flex items-center justify-center w-40 h-40 text-blue-200 bg-blue-900 shadow-lg opacity-75 cursor-move select-none">
          the text is select-none
      </div>
      <div class="flex items-center justify-center w-40 h-40 text-blue-200 bg-blue-900 shadow-lg opacity-75 select-all cursor-text">
          Select all text with one click
      </div>
      <div class="flex items-center justify-center w-40 h-40 text-blue-200 bg-blue-900 shadow-lg opacity-75 cursor-pointer select-auto">
          Select auto text
      </div>
    </div>
    """
  end
end
