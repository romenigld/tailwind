defmodule TailwindWeb.DisplayModesLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div>Hello <div class="w-32 h-6 bg-yellow-300">World!</div></div>
    <div>Hello <div class="inline w-32 h-6 bg-yellow-300">Minho!</div></div>
    <div>Hello <div class="inline-block w-32 h-6 bg-yellow-300">Romenig!</div></div>
    <div>Hello <div class="hidden w-32 h-6 bg-yellow-300">World!</div></div>
    """
  end
end
