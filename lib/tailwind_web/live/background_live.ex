defmodule TailwindWeb.BackgroundLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
      <div class="bg-indigo-400">
        Text
      </div>
      <div class="bg-green-300">
        Body
      </div>
      <div class="bg-blue-400">
        Footer
      </div>
    """
  end
end
