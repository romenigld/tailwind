defmodule TailwindWeb.FlexBoxLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <!-- Example 1 -->
      <div class="flex h-screen bg-green-300">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex justify-start">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex justify-center">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
        </div>
      <div class="flex justify-end">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex justify-between bg-cyan-500">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex items-end justify-around h-screen bg-blue-500">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex flex-row-reverse justify-center bg-green-300">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex flex-col-reverse justify-center bg-blue-300">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex flex-col justify-center bg-pink-300">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex flex-col items-center h-screen bg-yellow-300">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>
      <div class="flex flex-col items-center justify-center h-screen bg-green-600">
        <div class="w-16 h-16 bg-yellow-600">1</div>
        <div class="w-16 h-16 bg-teal-700">2</div>
        <div class="w-16 h-16 bg-red-700">3</div>
      </div>

      <!-- Example 2 -->
      <div class="flex">
          <div class="text-2xl bg-blue-500">&bull;</div>
          <p class="text-5xl ">Hello there.</p>
      </div>
      <div class="flex items-end">
          <div class="text-2xl bg-blue-500">&bull;</div>
          <p class="text-5xl ">Hello there.</p>
      </div>
      <div class="flex items-center">
          <div class="text-2xl bg-blue-500">&bull;</div>
          <p class="text-5xl ">Hello there.</p>
      </div>
      <div class="flex items-start">
          <div class="text-2xl bg-blue-500">&bull;</div>
          <p class="text-5xl ">Hello there.</p>
      </div>
      <div class="flex items-strecht">
          <div class="text-2xl bg-blue-500">&bull;</div>
          <p class="text-5xl ">Hello there.</p>
      </div>

      <!-- Flex Wrap -->
        <div class="flex">
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">7</div>
            <div class="w-16 h-16 bg-teal-700">8</div>
            <div class="w-16 h-16 bg-red-700">9</div>
        </div>
        <h2 class="text-4xl text-orange-500">Flex Wrap (resize the window for the effect).</h2>
        <div class="flex flex-wrap">
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">7</div>
            <div class="w-16 h-16 bg-teal-700">8</div>
            <div class="w-16 h-16 bg-red-700">9</div>
        </div>

        <h2 class="text-4xl text-orange-500">Flex Wrap Reverse(resize the window for the effect).</h2>
        <div class="flex flex-wrap-reverse">
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">1</div>
            <div class="w-16 h-16 bg-teal-700">2</div>
            <div class="w-16 h-16 bg-red-700">3</div>
            <div class="w-16 h-16 bg-yellow-600">7</div>
            <div class="w-16 h-16 bg-teal-700">8</div>
            <div class="w-16 h-16 bg-red-700">9</div>
        </div>
    """
  end
end
