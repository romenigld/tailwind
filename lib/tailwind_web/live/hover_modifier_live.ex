defmodule TailwindWeb.HoverModifierLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="flex items-center justify-center">
        <button class="px-4 py-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-600 hover:text-blue-500">Submit</button>
    </div>
    """
  end
end
