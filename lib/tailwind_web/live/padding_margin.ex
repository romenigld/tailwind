defmodule TailwindWeb.PaddingMarginLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="w-32 h-32 px-2 pt-4 mb-4 bg-blue-800">Text Text Text Text Text Text Text Text Text Text Text </div>
    <div class="w-32 h-32 p-4 bg-blue-500">Text</div>
    """
  end
end
