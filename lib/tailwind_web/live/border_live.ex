defmodule TailwindWeb.BorderLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="text-center">
      <h1 class="text-4xl text-purple-600 underline">Borders</h1>

      <div class="w-32 h-32 m-16 bg-gray-200 border-t-8 border-blue-800">
        &nbsp;
      </div>

      <div class="w-32 h-32 m-16 bg-gray-200 border-4 border-blue-800 border-dotted">
        &nbsp;
      </div>

      <div class="w-32 h-32 m-16 bg-gray-200 border-8 border-blue-800 border-dashed">
        &nbsp;
      </div>


      <div class="w-32 h-32 m-16 bg-gray-200 border-8 border-blue-800 border-double rounded-lg">
        &nbsp;
      </div>

      <div class="w-32 h-32 m-16 bg-gray-100 border-8 border-opacity-75 rounded-t-lg border-light-blue-800">&nbsp;</div>

      <div class="w-32 h-32 m-16 bg-gray-100 border border-blue-800 rounded-t-full">&nbsp;</div>

      <div class="w-32 h-32 m-16 bg-gray-100 border border-blue-800 rounded-bl-full">
        &nbsp;
      </div>
    </div>
    """
  end
end
