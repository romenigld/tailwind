defmodule TailwindWeb.ElementSizingLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="text-center">
      <h1>Element Sizing & Tailwnd's Numbering System</h1>
      <div class="w-32 h-32 bg-gray-500">w-32 h-32</div>
      <div class="w-1/2 bg-gray-400">1/2 half</div>
      <div class="w-screen bg-gray-300">screen</div>
      <div class="w-full bg-green-200">full</div>
      <div class="w-1/12 bg-red-300">1/12</div>
      <div class="w-2/12 bg-yellow-600">2/12</div>
      <div class="w-3/12 bg-red-200">3/12</div>
      <div class="w-6/12 bg-purple-200">6/12</div>
      <div class="w-1/3 bg-red-600">1/3</div>
      <div class="w-1/4 bg-red-500">1/4</div>
      <div class="w-1/5 bg-purple-300">1/5</div>
      <div class="w-1/6 bg-red-200">1/6</div>


      <h2>Shades of Blue Challenge</h2>
      <div class="w-5 h-6 bg-blue-100">Text</div>
      <div class="w-5 h-6 bg-blue-200">Text</div>
      <div class="w-5 h-6 bg-blue-300">Text</div>
      <div class="w-5 h-6 bg-blue-400">Text</div>
      <div class="w-5 h-6 bg-blue-500">Text</div>
      <div class="w-5 h-6 bg-blue-600">Text</div>
      <div class="w-5 h-6 bg-blue-700">Text</div>
      <div class="w-5 h-6 bg-blue-800">Text</div>
      <div class="w-5 h-6 bg-blue-900">Text</div>
    </div>
    """
  end
end
