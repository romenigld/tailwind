defmodule TailwindWeb.FocusModifierLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="flex items-center justify-center p-6 bg-gray-200">
      <input class="block w-full px-4 py-2 bg-white border border-gray-300 rounded-lg sm:hover:bg-red-500 focus:bg-blue-200 focus:text-yellow-200" type="email" placeholder="jane@example.com">
    </div>
    """
  end
end
