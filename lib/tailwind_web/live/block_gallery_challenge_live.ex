defmodule TailwindWeb.BlockGalleryChallengeLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <!-- Must take up all of the available space -->
    <!-- Evenly distributed 3x3 grid -->
    <!-- Letters must be centered on the square -->
    <!-- Must have some spacing between the blocks -->
    <div class="flex flex-wrap h-screen">
      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-100">A</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-200">B</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-300">C</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-400">D</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-500">E</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-600">F</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-700">G</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-800">H</div>
      </div>

      <div class="flex w-1/3 h-1/3">
        <div class="flex items-center justify-center w-full m-2 bg-teal-900">I</div>
      </div>
    </div>
    """
  end
end
