defmodule TailwindWeb.EmployeeCardChallengeLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="flex items-center justify-center h-screen bg-gray-600">
      <div class="flex flex-col w-2/3 overflow-hidden bg-white rounded-lg shadow-lg">
        <div class="px-6 py-4 text-lg text-gray-700 bg-gray-200">
          The title of the card here
        </div>

        <div class="flex items-center justify-between px-6 py-4">
            <div class="px-2 py-1 text-xs font-bold text-gray-200 uppercase bg-orange-600 border border-gray-200 rounded-full">Under Review</div>
            <div class="text-sm">May 14, 1988</div>
        </div>

        <div class="px-6 py-4 border-t border-gray-200">
            <div class="p-4 bg-gray-200 border rounded-lg">
                Here is a short comment about this employee.
            </div>
        </div>

        <div class="px-6 py-4 bg-gray-200">
            <div class="text-xs font-bold text-gray-600 uppercase">
              Employee
            </div>

            <div class="flex items-center pt-3">
                <div class="flex items-center justify-center w-12 h-12 font-bold text-white uppercase bg-blue-700 rounded-full">
                  VG
                </div>
                <div class="ml-4">
                    <p class="font-bold">Victor Gonzalez</p>
                    <p class="mt-1 text-sm text-gray-700">Instructor</p>
                </div>
            </div>
        </div>
      </div>
    </div>
    """
  end
end
