defmodule TailwindWeb.InputComponentChallengeLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="flex flex-col items-center justify-center h-screen">

    <label for="full_name" class="text-sm font-bold text-gray-700 select-none">Full Name</label>
    <input id="full_name"
          placeholder="Enter your full name"
          class="w-4/6 px-3 py-2 mt-2 text-gray-700 placeholder-indigo-300 border rounded-lg shadow focus:bg-blue-100">
    <button class="px-3 py-2 mt-2 text-blue-100 bg-blue-900 rounded-lg">Save</button>
    </div>
    """
  end
end
