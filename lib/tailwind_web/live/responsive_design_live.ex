defmodule TailwindWeb.ResponsiveDesignLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="flex flex-col bg-blue-500 sm:flex-row">
      <div class="p-4 sm:font-bold md:italic">
        Some text inside this div with Tailwind CSS
      </div>
      <div class="p-4 sm:border">
        Other text inside this div with Tailwind CSS
      </div>
      <div class="w-32 h-32 bg-gray-200 ">A</div>
      <div class="w-32 h-32 bg-gray-200 ">B</div>
    </div>
    """
  end
end
