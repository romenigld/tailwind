defmodule TailwindWeb.ButtonsChallengeLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <div class="p-3 m-3 text-center">
      <h1 class="p-6 text-4xl text-blue-500 uppercase">Buttons</h1>

      <!-- blue background, rounded, thick left border -->
      <div><button class="px-4 py-2 m-4 text-sm text-gray-200 bg-blue-800 border-l-8 border-blue-400 rounded-lg">Submit</button></div>

      <!-- Red text, outlined -->
      <div><button class="px-3 py-1 m-4 text-red-500 border-2 border-red-500 rounded">Cancel</button></div>

      <!-- light Indigo background, bottom border, indigo text -->
      <div><button class="px-4 py-1 m-4 bg-indigo-200 border-t-4 border-b-4 border-indigo-800">Save</button></div>

      <!-- Thick rounded, lots of padding, large button -->
      <div><button class="px-16 py-3 m-4 text-sm font-bold uppercase bg-green-600 border-4 border-green-800 rounded-full">Buy Now</button></div>

      <!-- Outlined, serif font, uppercase, rounded -->
      <div><button class="p-2 m-4 font-serif text-xs uppercase border rounded-lg bg-cyan-400 border-true-gray-500 text-purple-50">Send Postcard</button></div>
    </div>
    """
  end

end
