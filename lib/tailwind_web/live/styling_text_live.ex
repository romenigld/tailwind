defmodule TailwindWeb.StylingTextLive do
  use TailwindWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <p class="p-4 m-4 font-serif text-base tracking-wider text-right text-blue-500 underline leading-relax">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      <span class="font-extrabold no-underline uppercase">Nullam suscipit orci ac nisl varius varius.</span>
      Nullam auctor finibus pulvinar. Morbi porttitor placerat enim nec consequat.
    </p>
    <p class="p-6 m-3 font-mono text-xs leading-snug tracking-tight text-center text-gray-500 capitalize">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam suscipit orci ac nisl varius varius.
      Nullam auctor finibus pulvinar. Morbi porttitor placerat enim nec consequat.
      <span class="font-bold line-through uppercase">Nullam suscipit orci ac nisl varius varius.</span>
    </p>
    <p class="p-5 m-3 font-sans text-xl italic leading-loose tracking-widest text-justify text-purple-500 capitalize bg-gray-300">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      <span class="not-italic font-black underline normal-case"> Nullam suscipit orci ac nisl varius varius.</span>
      Nullam auctor finibus pulvinar. Morbi porttitor placerat enim nec consequat.
    </p>

    <h2 class="text-2xl text-center text-green-300">Challenge Text Styling</h2>

    <!-- Title case heading 1 text in dark gray -->
    <h1 class="text-xl text-gray-900 capitalize">Lorem ipsum dolor sit amet consectetur.</h1>

    <!-- Paragraph styling with 1.5 line heights & letter spacing -->
    <p class="mt-3 leading-loose tracking-wide">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam suscipit orci ac nisl varius varius. Nullam auctor finibus pulvinar. Morbi porttitor placerat enim nec consequat.
    </p>

    <!-- Treated like a quote with a background and plenty of padding. Italic title -->
    <div class="p-4 m-2 text-blue-200 bg-blue-900">
      <p class="font-serif italic">
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam suscipit orci ac nisl varius varius. Nullam auctor finibus pulvinar. Morbi porttitor placerat enim nec consequat."
      </p>
      <p class="mt-3 font-mono text-right text-gray-400">- Romenig Lima Damasio, <span class="italic">instructor</span></p>

      <!-- Call to action button, blue with good padding and uppercase -->
      <button class="px-4 py-3 text-blue-100 uppercase bg-blue-500">Enroll Now</button>
    </div>
    """
  end
end
