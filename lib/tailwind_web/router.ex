defmodule TailwindWeb.Router do
  use TailwindWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {TailwindWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TailwindWeb do
    pipe_through :browser

    live "/", PageLive, :index
    live "/background", BackgroundLive
    live "/element-sizing", ElementSizingLive
    live "/padding-margin", PaddingMarginLive
    live "/styling-text", StylingTextLive
    live "/border", BorderLive
    live "/buttons-challenge", ButtonsChallengeLive
    live "/display-modes", DisplayModesLive
    live "/flexbox", FlexBoxLive
    live "/block-gallery-challenge", BlockGalleryChallengeLive
    live "responsive-design", ResponsiveDesignLive
    live "/hover-modifier", HoverModifierLive
    live "/focus-modifier", FocusModifierLive
    live "/other-utilities", OtherUtilitiesLive
    live "/input-component-challenge", InputComponentChallengeLive
    live "/employee-card-challenge", EmployeeCardChallengeLive
    live "/landing-page", LandingPageLive
  end

  # Other scopes may use custom stacks.
  # scope "/api", TailwindWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: TailwindWeb.Telemetry
    end
  end
end
